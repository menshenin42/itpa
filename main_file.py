"""
    Program for forth lab in introduction to
    professional activity
    Variant 9, About palindromes
    Menshenin Andrew KI19-16/2b
"""


def input_check(string):
    """
    This function checks if the user's input is correct.

     Args:
        string: inputed string.

    Returns:
        bool value: True if all is correct; False if something is wrong.

    Examples:
        >>> input_check("123")
        True
        >>> input_check("-123")
        False
        >>> input_check("12.3")
        False
        >>> input_check("0")
        False
        >>> input_check("dfgfnh`12312")
        False
    """
    try:
        if float(string) == int(string):
            number = int(string)
            if number > 0:
                return True
            else: 
                return False
        else:
            return False
    except ValueError:
        return False


def reverse_and_sum(number):
    """
    Service function to reverse and sum a number for a single iteration.

    Args:
        number: a number to reverse and sum to itself

    Returns:
        number + reversed number

    Examples:
        >>> reverse_and_sum(123)
        444
        >>> reverse_and_sum(555)
        1110
        >>> reverse_and_sum(356)
        1009
    """
    return str(int(number)+int(''.join(reversed(str(number)))))


def determination(number):
    """
    A function to determine if a number becomes a palindrome after 100 (or less)
    reversing and summing to itself.

    Args:
        number: a number you what to examine

    Returns:
        bool, int
        bool - True if it became a palindrome; False if didn't
        int - an iteration when it became a palindrome

    Examples:
        >>> determination(493)
        False, None
        >>> determination(592)
        False, None
        >>> determination(1)
        True, 0
        >>> determination(23)
        True, 1

    """
    for i in range(100):
    
        pal, _, _ = palindrome(str(number))
        if pal == 1:
            return True, i
            
        number = reverse_and_sum(number)
        
    return False, None


def palindrome(string):
    """
    A function witch determines if a string is a palindrome.

    Also it determines if it is a almost-palindrome: there is one digit to change in string
    for it to become a palindrome.

    Args:
        string: the examined string.

    Returns:
        bool, int, int
        bool - True: palindrome; False: not
        int1, int2 - indexes of missmatching chars.

    Examples:
        >>> palindrome("qwerrewq")
        1, None, None
        >>> palindrome("sadfd")
        0, None, None
        >>> palindrome("qweriewq")
        -1, "r", "i"
    """
    if string == ''.join(reversed(string)):
        return 1, None, None

    else:
        count_of_mismatching = 0
        mismatching_letter1 = ''
        mismatching_letter2 = ''

        for i in range(len(string) // 2):
            if string[i] != string[-i - 1]:
                count_of_mismatching += 1
                mismatching_letter1 = string[i]
                mismatching_letter2 = string[-i - 1]

                if count_of_mismatching > 1:
                    return 0, None, None

        return -1, mismatching_letter1, mismatching_letter2


def main():
    inp = input()
    if not input_check(inp):
        print("Invalid input")
    else:
        inp = int(inp)
        pal, i = determination(inp)

        if pal:
            print(pal, i)
        else:
            print("Nope")

    print("1, 1")


if __name__ == "__main__":
    main()
