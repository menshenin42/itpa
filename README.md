# Practical work 5

This is a program for 5-th practical work on introduction to
poffrcional activity.

There was two tasks:
1. to determinate if a string is a **palindrome** or a __half-palindrome__
2. to determinate if a number becomes a palindrome in 100 iterations 
of reverse-and-sum action

The first tusk is done by 'palindrome' function.
It terurns three vaslues.
First is a bool value: palindrome or not.
Two other are missmatching chars in the string. If it is a palindrome than
two last values are None.

The second task is done by 'determination' function.
It accepts an integer to examine it's palindromeability.
Returns two values: bool (if it becomes a pal. or not) and
int (number of iterations)

How it works:
    We do reverse-and-sum for a 100 times and check the number using the 
    palindrome. If it is a pal on some iteration then we are done.
    ufter the cycle we can return False value.

If you dont understand reverse-and-sum here is it's function:
```
def reverse_and_sum(number):
    return str(int(number)+int(''.join(reversed(str(number)))))
```

Alsow the tusk requiers a marked list, so here it is:
- line 1
- line 2

I found how to do it on [this link](https://help.github.com/en/github/writing-on-github/basic-writing-and-formatting-syntax#quoting-code).

some cind of formula:
```math
E = m^2*c+d
```
    
<picture>
 <source srcset="https://sun9-15.userapi.com/c855432/v855432162/fe469/15MFNcBVFHA.jpg" media="(min-width: 100px)">
 <img src="https://sun9-15.userapi.com/c855432/v855432162/fe469/15MFNcBVFHA.jpg" alt="MDN">
</picture>