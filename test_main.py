from main_file import input_check
from main_file import palindrome
from main_file import determination
from main_file import reverse_and_sum


# input check, all cases
def test_input_check():
    assert input_check("123") is True
    assert input_check("-123") is False
    assert input_check("12.3") is False
    assert input_check("0") is False
    assert input_check("dfgfnh`12312") is False


# reverse and sum, all cases
def test_reverse_and_sum():
    assert reverse_and_sum(123) == 444
    assert reverse_and_sum(555) == 1110
    assert reverse_and_sum(356) == 1009


# palindrome func test, all cases
def test_palindrome():
    assert palindrome("qwerrewq") == (1, None, None)
    assert palindrome("sadfd") == (0, None, None)
    assert palindrome("qweriewq") == (-1, "r", "i")
    

# determination func test, all possible cases
def test_determination():
    assert determination(196) == (False, None)
    assert determination(295) == (False, None)
    assert determination(394) == (False, None)
    assert determination(493) == (False, None)
    assert determination(592) == (False, None)
    assert determination(1) == (True, 0)
    assert determination(23) == (True, 1)
    assert determination(322) == (True, 1)
    assert determination(228) == (True, 2)
    assert determination(1488) == (True, 3)
